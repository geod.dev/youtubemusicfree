const API_KEY = "YOUTUBE API KEY HERE";
const API_URL = "https://www.googleapis.com/youtube/v3/";

async function get(route, params) {
    let url = API_URL + route + "?"
    params = params.concat(["key=" + API_KEY]);
    params.forEach(p => url += p + "&");
    return await fetch(url)
        .then(response => response.json());
}

// export function get(route, params) {
//     let request = new XMLHttpRequest();
//     request.open('GET', getUrl(route, params), true);
//
//     let result;
//     request.onload = function () {
//         if (this.status >= 200 && this.status < 400) {
//             result = JSON.parse(this.response);
//         } else {
//             result = null;
//         }
//     };
//
//     request.send();
//     return result;
// }