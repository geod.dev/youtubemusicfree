const btn = document.getElementById("play");
const infos = document.querySelector(".player .info");
const slider = document.querySelector(".player .time .slider");
const timer = document.querySelector(".player .time .timer");
const maxTimer = document.querySelector(".player .time .maxTime");

let player, time, maxTime, timeUpdater;

export function loadVideo(id) {
    let params = ["id=" + id, "part=snippet,contentDetails"];
    get("videos", params).then(data => {
        let video = data.items[0];
        if (video !== undefined) {
            if (player === undefined) {
                player = new YT.Player('yt', {
                    height: '390', width: '640', videoId: video.id, playerVars: {
                        'playsinline': 1
                    }, events: {
                        'onReady': playerReady,
                        'onStateChange': refreshButton,
                    }
                });
            } else {
                player.loadVideoById(video.id)
            }
            refreshInfos(
                "https://img.youtube.com/vi/" + id + "/mqdefault.jpg",
                video.snippet.title,
                video.snippet.channelTitle
            )
            maxTime = convertISO8601ToSeconds(video.contentDetails.duration);
        } else {
            document.querySelector(".search-text").innerHTML = "Une erreur s'est produite";
        }
    })
}

function playerReady() {
    player.playVideo();

    function updateTime() {
        var oldTime = time;
        if (player && player.getCurrentTime) {
            time = player.getCurrentTime();
        }
        if (time !== oldTime) refreshTime(time);
    }

    timeUpdater = setInterval(updateTime, 100);
}

function refreshButton(event) {
    if (btn !== null) {
        if (event.data === 1) {
            btn.className = "fas fa-pause"
        } else if (event.data === 2) {
            btn.className = "fas fa-play"
        } else {
            btn.className = "fas fa-loader"
        }
    }
}

function togglePlay() {
    if (btn !== null && player !== null) {
        if (player.getPlayerState() === 1) {
            player.pauseVideo();
        } else if (player.getPlayerState() === 2) {
            player.playVideo();
        }
    }
}

btn.onclick = togglePlay;

function refreshInfos(link, title, author) {
    if (infos === null) return
    infos.children[0].src = link;
    infos.children[1].children[0].innerHTML = title;
    infos.children[1].children[1].innerHTML = author;

}

function refreshTime(current) {
    current = Math.trunc(current)
    timer.innerHTML = (current + '').toHHMMSS();
    slider.value = current;
    slider.max = maxTime;
    maxTimer.innerHTML = (maxTime + '').toHHMMSS();
}

slider.onchange = function setTime() {
    if (player !== null) player.seekTo(Math.trunc(slider.value))
}

// SEARCH

window.onclick = e => {
    let id = getParentIdByClass(e.target, "result")
    if (id != null) loadVideo(id);
}

function getParentIdByClass(element, specificClass) {
    while(element.parentNode && element.parentNode.nodeName.toLowerCase() !== 'body') {
        element = element.parentNode;
        console.log(element)
        console.log(element.className.split(' ').indexOf(specificClass))
        if (element.className.split(' ').indexOf(specificClass) >= 0) {
            return element.id;
        }
    }
    return null;
}