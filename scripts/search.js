const input = document.querySelector(".search-bar");
const text = document.querySelector(".search-text");
const btn = document.querySelector(".search-btn");
const results = document.querySelector(".results");

let params;

function search(keyword) {
    params = ["q=" + keyword, "maxResults=50", "videoCategoryId=10", "type=video", "regionCode=FR", "part=snippet"];
    text.innerHTML = "Resultat de recherches pour <strong>" + keyword + "</strong>";
    results.innerHTML = "";
    get("search", params).then(data => {
        for (const index in data.items) {
            let video = data.items[index];
            console.log(video)
            addResult(video.id.videoId, video.snippet.title, video.snippet.channelTitle)
        }
    })
}

function inputSearch() {
    if (input.value !== "") search(input.value)
}

btn.onclick = inputSearch;

function addResult(id, title, author) {
    let div = document.createElement("div")
    div.classList.add("result")
    div.id = id;
    let text = document.createElement("div")
    text.classList.add("result-text")
    let thumbnail = document.createElement("img");
    thumbnail.classList.add("result-img")
    thumbnail.src = "https://img.youtube.com/vi/" + id + "/mqdefault.jpg"
    let pTitle = document.createElement("p");
    pTitle.classList.add("result-title")
    pTitle.innerText = title
    let pAuthor = document.createElement("p");
    pAuthor.classList.add("result-author")
    pAuthor.innerText = author
    // let pDuration = document.createElement("p");
    // thumbnail.classList.add("result-duration")
    // duration = convertISO8601ToSeconds(duration);
    // pDuration.innerText = duration.toHHMMSS();

    div.appendChild(thumbnail);
    div.appendChild(text)
    text.appendChild(pTitle);
    text.appendChild(pAuthor);
    // div.appendChild(pDuration);
    results.appendChild(div);
}