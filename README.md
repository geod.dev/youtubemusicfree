# YoutubeMusicFree
Simple test project who get and play musics from youtube

## Install

Juste clone the project and set **API_KEY** in **scripts/api.js** (first line)
###### [How to get a Youtube Api V3 Key](https://rapidapi.com/blog/how-to-get-youtube-api-key/)

## License
No licence, its free and open. Do everything you want.